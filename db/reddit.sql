CREATE TABLE IF NOT EXISTS `posts` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`post_id`	TEXT NOT NULL UNIQUE,
	`title`	TEXT,
	`url`	TEXT,
	`post_time`	TEXT,
	`subreddit` TEXT
);
CREATE TABLE IF NOT EXISTS `avatar` (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `image_url` TEXT NOT NULL UNIQUE,
    `subreddit` TEXT NOT NULL
);